﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using WebsiteRanking.Api.Models;
using WebsiteRanking.Api.Repository.Connection;

namespace WebsiteRanking.Api.Repository
{
    public class DatabaseContext : DbContext
    {
        private readonly IDbConnection _dbConnection;

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Entity<WebsiteStats>().ToTable("WebsiteStats");
        }

        public DatabaseContext(IConnectionFactory connectionFactory)
            : base("DefaultConnection")
        {
            _dbConnection = connectionFactory.Create();
        }

        protected override void Dispose(bool disposing)
        {
            if (_dbConnection.State == ConnectionState.Open)
            {
                _dbConnection.Close();
            }
            _dbConnection.Dispose();

            base.Dispose(disposing);

        }

    }
}