﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace WebsiteRanking.Api.Repository.Connection
{
    public class ConnectionFactory : IConnectionFactory
    {
        private readonly DbProviderFactory _dbProvider;
        private readonly string _connectionString;

        public ConnectionFactory(ConnectionStringSettings connectionSettings)
        {
            _connectionString = connectionSettings.ConnectionString;
            _dbProvider = DbProviderFactories.GetFactory(connectionSettings.ProviderName);
        }

        public IDbConnection Create()
        {
            var connection = _dbProvider.CreateConnection();
            if (connection == null)
            {
                var message = $"Failed to create a connection using the connection string {_connectionString} in app/web.config,";
                throw new ConfigurationErrorsException(message);
            }

            connection.ConnectionString = _connectionString;
            connection.Open();

            return connection;
        }
    }
}