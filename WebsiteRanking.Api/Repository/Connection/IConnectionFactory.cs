﻿using System.Data;

namespace WebsiteRanking.Api.Repository.Connection
{
    public interface IConnectionFactory
    {
        IDbConnection Create();
    }
}
