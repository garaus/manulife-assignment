﻿namespace WebsiteRanking.Api.Repository.EFRepository
{
    public interface IEntity<TKey>
    {
        TKey Id { get; set; }
    }
}
