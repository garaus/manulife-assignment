﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace WebsiteRanking.Api.Repository.EFRepository
{
    public class EFRepository<T, TKey> : IRepository<T, TKey> where T : class, IEntity<TKey>
    {
        private readonly DbSet<T> _dbSet;
        private readonly DatabaseContext _dbContext;

        public EFRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<T>();
        }

        public T Get(Func<T, bool> predicate)
        {
            IQueryable<T> query = _dbSet;
            return query.FirstOrDefault(predicate);
        }

        public IEnumerable<T> GetAll(Func<T, bool> predicate = null)
        {
            if (predicate != null)
            {
                return _dbSet.Where(predicate);
            }
            return _dbSet;
        }

        public void Add(T entity)
        {
            _dbSet.Add(entity);
        }

        public void Update(T entity)
        {
            _dbSet.Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            if (_dbContext.Entry(entity).State == EntityState.Detached)
            {
                _dbSet.Attach(entity);
            }
            _dbSet.Remove(entity);
        }

        public int Commit()
        {
            return _dbContext.SaveChanges();
        }
    }
}