﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebsiteRanking.Api.Models;
using WebsiteRanking.Api.Repository.EFRepository;

namespace WebsiteRanking.Api.Repository
{
    public class WebsiteRepo : EFRepository<WebsiteStats, int>, IWebsiteRepo
    {
        private DatabaseContext _ctx;
        public WebsiteRepo(DatabaseContext dbContext) : base(dbContext)
        {
            _ctx = dbContext;
        }
    }
}