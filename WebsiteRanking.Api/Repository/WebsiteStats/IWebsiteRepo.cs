﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebsiteRanking.Api.Models;

namespace WebsiteRanking.Api.Repository
{
    public interface IWebsiteRepo : IRepository<WebsiteStats, int>
    {
    }
}
