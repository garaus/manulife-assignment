﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WebsiteRanking.Api.Repository.EFRepository;

namespace WebsiteRanking.Api.Repository
{
    public interface IRepository<T, TKey> where T : class, IEntity<TKey>
    {
        IEnumerable<T> GetAll(Func<T, bool> predicate = null);

        T Get(Func<T, bool> predicate);

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);

        int Commit();
    }
}
