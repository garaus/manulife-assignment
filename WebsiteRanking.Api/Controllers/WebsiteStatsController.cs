﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebsiteRanking.Api.Models;
using WebsiteRanking.Api.Repository;

namespace WebsiteRanking.Api.Controllers
{
    public class WebsiteStatsController : ApiController
    {
        private IWebsiteRepo _repo;
         
        public WebsiteStatsController(IWebsiteRepo repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public Task<IEnumerable<WebsiteStats>> Get()
        {
            IEnumerable<WebsiteStats> results = _repo.GetAll();

            return Task.FromResult(results);
        }
    }
}