﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using WebsiteRanking.Api.Repository.EFRepository;

namespace WebsiteRanking.Api.Models
{
    public class WebsiteStats : IEntity<int>
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Website { get; set; }
        public int Visits { get; set; }
    }
}