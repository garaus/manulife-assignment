using Microsoft.Practices.Unity;
using System;
using System.Configuration;
using System.Web.Http;
using Unity.WebApi;
using WebsiteRanking.Api.Repository;
using WebsiteRanking.Api.Repository.Connection;

namespace WebsiteRanking.Api
{
    public static class UnityConfig
    {
        public static void RegisterComponents(HttpConfiguration config)
        {
            var container = new UnityContainer()
                .RegisterInstance(GetConnectionSettings())
                .RegisterType<IConnectionFactory, ConnectionFactory>()
                .RegisterType<IWebsiteRepo, WebsiteRepo>();

            config.DependencyResolver = new UnityDependencyResolver(container);
        }

        private static ConnectionStringSettings GetConnectionSettings()
        {
            var connectionSettings = ConfigurationManager.ConnectionStrings["DefaultConnection"];
            if (connectionSettings == null)
            {
                throw new InvalidOperationException("Connection settings does not exist.");
            }

            return connectionSettings;
        }

    }
}