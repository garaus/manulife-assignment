import { Component } from '@angular/core';
import {Http} from "@angular/http";
import {WebsiteStats} from "./WebsiteStats";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  days: Array<string>;
  websites: Array<WebsiteStats>;
  constructor(private http: Http) {
      http.get('http://localhost:59792/api/getVisitingDays')
        .subscribe(res => {
          this.days = res.json();
        })
  }

  getWebsites(day: string) {
    this.http.get('http://localhost:59792/api/websitestats')
      .subscribe(res => {
        this.websites = res.json();
      })
  }

}
