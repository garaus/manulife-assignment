export class WebsiteStats {
  constructor(public id: number, public website: string, public date: string, public visitors: number){
  }
}
