import { WebsiteRankingWebPage } from './app.po';

describe('website-ranking-web App', function() {
  let page: WebsiteRankingWebPage;

  beforeEach(() => {
    page = new WebsiteRankingWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
