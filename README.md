# README #

Manulife Top Website ranking application assignment.

### What is this repository for? ###

* Application is far from completion as it requires a significant investment of time. However, still technical approach and patterns can be over viewed. 
Thus, in the application was implemented common repository pattern that can support CRUD operations for any entity. Every entity repository implementation can be easily 
extended with optional logic.  
* Angular2 client application is not in the working condition. It has only couple http calls to the server to retrieve data as well as data storage file. However, calls are 
not completed.
* Version 0.0.0.1


### How do I get set up? ###

* WebApi back end application's packages will be restored from nuget. 
* Database folder contains a .dacpac file for an MS SQL Database. 
* Restore database by importing Data Tier Application from servers contextual menu. 
* In web.config change connection string to an appropriate for your server settings.

Angular2 client application will require angilar-cli npm package installed globally on a testing workstation. After that go to WebsiteRanking.Web project folder and run 
```
npm install
```
* To run client application from inside *.Web project folder run 
```
ng serve
```